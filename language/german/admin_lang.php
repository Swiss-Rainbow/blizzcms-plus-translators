<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Navbar Lang*/
$lang['admin_nav_dashboard'] = 'Dashboard';
$lang['admin_nav_system'] = 'System';
$lang['admin_nav_manage_settings'] = 'Manage Einstellungen';
$lang['admin_nav_manage_modules'] = 'Module bearbeiten';
$lang['admin_nav_users'] = 'Nutzer';
$lang['admin_nav_accounts'] = 'Accounts';
$lang['admin_nav_website'] = 'Website';
$lang['admin_nav_menu'] = 'Menu';
$lang['admin_nav_realms'] = 'Realms';
$lang['admin_nav_slides'] = 'Slides';
$lang['admin_nav_news'] = 'Neuigkeiten';
$lang['admin_nav_changelogs'] = 'Änderungsprotokoll';
$lang['admin_nav_pages'] = 'Seiten';
$lang['admin_nav_donate_methods'] = 'Methoden Spenden';
$lang['admin_nav_topsites'] = 'Topseiten';
$lang['admin_nav_donate_vote_logs'] = 'Spenden/Abstimmung Protokolle';
$lang['admin_nav_store'] = 'Shop';
$lang['admin_nav_manage_store'] = 'Shop bearbeiten';
$lang['admin_nav_forum'] = 'Forum';
$lang['admin_nav_manage_forum'] = 'Foren bearbeiten';
$lang['admin_nav_logs'] = 'Protokolle System';

/*Sections Lang*/
$lang['section_general_settings'] = 'Allgemeine Einstellungen';
$lang['section_module_settings'] = 'Module Einstellungen';
$lang['section_optional_settings'] = 'Optionale Einstellung';
$lang['section_seo_settings'] = 'SEO Einstellung';
$lang['section_update_cms'] = 'Update CMS';
$lang['section_check_information'] = 'Informationen überprüfen';
$lang['section_forum_categories'] = 'Forum Kategorien';
$lang['section_forum_elements'] = 'Forum Elemente';
$lang['section_store_categories'] = 'Shop Kategorien';
$lang['section_store_items'] = 'Shop Items';
$lang['section_store_top'] = 'Shop TOP Items';
$lang['section_logs_dp'] = 'Spende Protokolle';
$lang['section_logs_vp'] = 'Abstimmung Protokolle';

/*Button Lang*/
$lang['button_select'] = 'wählen';
$lang['button_update'] = 'Update';
$lang['button_unban'] = 'Unban';
$lang['button_ban'] = 'Ban';
$lang['button_remove'] = 'Entfernen';
$lang['button_grant'] = 'Gewähren';
$lang['button_update_version'] = 'Update auf die neueste Version';

/*Table header Lang*/
$lang['table_header_subcategory'] = 'Wähle eine Unterkategorie';
$lang['table_header_race'] = 'Rasse';
$lang['table_header_class'] = 'Klasse';
$lang['table_header_level'] = 'Level';
$lang['table_header_money'] = 'vermögen';
$lang['table_header_time_played'] = 'Zeit gespielt';
$lang['table_header_actions'] = 'Aktionen';
$lang['table_header_id'] = '#ID';
$lang['table_header_tax'] = 'MwSt';
$lang['table_header_points'] = 'Punkte';
$lang['table_header_type'] = 'Type';
$lang['table_header_module'] = 'Module';
$lang['table_header_payment_id'] = 'Payment ID';
$lang['table_header_hash'] = 'Hash';
$lang['table_header_total'] = 'Total';
$lang['table_header_create_time'] = 'Zeit schaffen';
$lang['table_header_guid'] = 'Guid';
$lang['table_header_information'] = 'Information';
$lang['table_header_value'] = 'Wert';

/*Input Placeholder Lang*/
$lang['placeholder_manage_account'] = 'Konto verwalten';
$lang['placeholder_update_information'] = 'Kontoinformation aktualisieren';
$lang['placeholder_donation_logs'] = 'Spenden Protokolle';
$lang['placeholder_store_logs'] = 'Shop Protokolle';
$lang['placeholder_create_changelog'] = 'Erstellen Änderungsprotokoll';
$lang['placeholder_edit_changelog'] = 'Bearbeiten Änderungsprotokoll';
$lang['placeholder_create_category'] = 'Erstellen Kategorie';
$lang['placeholder_edit_category'] = 'Bearbeiten Kategorie';
$lang['placeholder_create_forum'] = 'Erstellen Forum';
$lang['placeholder_edit_forum'] = 'Bearbeitenn Forum';
$lang['placeholder_create_menu'] = 'Erstellen Menu';
$lang['placeholder_edit_menu'] = 'Bearbeiten Menu';
$lang['placeholder_create_news'] = 'Erstellen News';
$lang['placeholder_edit_news'] = 'Bearbeiten News';
$lang['placeholder_create_page'] = 'Erstellen Seite';
$lang['placeholder_edit_page'] = 'Bearbeiten Seite';
$lang['placeholder_create_realm'] = 'Erstellen Realm';
$lang['placeholder_edit_realm'] = 'Bearbeiten Realm';
$lang['placeholder_create_slide'] = 'Erstellen Slide';
$lang['placeholder_edit_slide'] = 'Bearbeiten Slide';
$lang['placeholder_create_item'] = 'Erstellen item';
$lang['placeholder_edit_item'] = 'Bearbeiten Item';
$lang['placeholder_create_topsite'] = 'Erstellen Topseite';
$lang['placeholder_edit_topsite'] = 'Bearbeiten Topseite';
$lang['placeholder_create_top'] = 'Erstellen TOP Item';
$lang['placeholder_edit_top'] = 'Bearbeiten TOP Item';

$lang['placeholder_upload_image'] = 'Bild hochladen';
$lang['placeholder_icon_name'] = 'Symbolname';
$lang['placeholder_category'] = 'Kategorie';
$lang['placeholder_name'] = 'Name';
$lang['placeholder_item'] = 'Item';
$lang['placeholder_image_name'] = 'Bildname';
$lang['placeholder_reason'] = 'Grund';
$lang['placeholder_gmlevel'] = 'GM Level';
$lang['placeholder_url'] = 'URL';
$lang['placeholder_child_menu'] = 'Kindermenü';
$lang['placeholder_url_type'] = 'URL Art';
$lang['placeholder_route'] = 'Route';
$lang['placeholder_hours'] = 'Std';
$lang['placeholder_soap_hostname'] = 'Soap Hostname';
$lang['placeholder_soap_port'] = 'Soap Port';
$lang['placeholder_soap_user'] = 'Soap Benutzer';
$lang['placeholder_soap_password'] = 'Soap Passwort';
$lang['placeholder_db_character'] = 'Character';
$lang['placeholder_db_hostname'] = 'Datenbank Hostname';
$lang['placeholder_db_name'] = 'Datenbank Name';
$lang['placeholder_db_user'] = 'Datenbank Benutzer';
$lang['placeholder_db_password'] = 'Datenbank Passwort';
$lang['placeholder_account_points'] = 'Konto Punkte';
$lang['placeholder_account_ban'] = 'Ban Konto';
$lang['placeholder_account_unban'] = 'Unban Konto';
$lang['placeholder_account_grant_rank'] = 'Gewähren GM Rang';
$lang['placeholder_account_remove_rank'] = 'Entfernen GM Rang';
$lang['placeholder_command'] = 'Befehl';

/*Config Lang*/
$lang['conf_website_name'] = 'Webseite Name';
$lang['conf_realmlist'] = 'Realmlist';
$lang['conf_discord_invid'] = 'Discord Einladung ID';
$lang['conf_timezone'] = 'Zeitzone';
$lang['conf_theme_name'] = 'Theme Name';
$lang['conf_maintenance_mode'] = 'Wartungsmodus';
$lang['conf_social_facebook'] = 'Facebook URL';
$lang['conf_social_twitter'] = 'Twitter URL';
$lang['conf_social_youtube'] = 'Youtube URL';
$lang['conf_paypal_currency'] = 'PayPal Währung';
$lang['conf_paypal_mode'] = 'PayPal Modus';
$lang['conf_paypal_client'] = 'PayPal Client ID';
$lang['conf_paypal_secretpass'] = 'PayPal Geheimes Passwort';
$lang['conf_default_description'] = 'Standardbeschreibung';
$lang['conf_admin_gmlvl'] = 'Administrator GMLevel';
$lang['conf_mod_gmlvl'] = 'Moderator GMLevel';
$lang['conf_recaptcha_key'] = 'reCaptcha Site Key';
$lang['conf_account_activation'] = 'Account Aktivierung';
$lang['conf_smtp_hostname'] = 'SMTP Hostname';
$lang['conf_smtp_port'] = 'SMTP Port';
$lang['conf_smtp_encryption'] = 'SMTP Verschlüsselung';
$lang['conf_smtp_username'] = 'SMTP Nutzername';
$lang['conf_smtp_password'] = 'SMTP Passwort';
$lang['conf_sender_email'] = 'Sender Email';
$lang['conf_sender_name'] = 'Sender Name';

/*Logs */
$lang['placeholder_logs_dp'] = 'Spende';
$lang['placeholder_logs_quantity'] = 'Menge';
$lang['placeholder_logs_hash'] = 'Hash';
$lang['placeholder_logs_voteid'] = 'Abstimmung ID';
$lang['placeholder_logs_points'] = 'Punkte';
$lang['placeholder_logs_lasttime'] = 'Letztes Mal';
$lang['placeholder_logs_expiredtime'] = 'Abgelaufene Zeit';

/*Status Lang*/
$lang['status_completed'] = 'Abgeschlossen';
$lang['status_cancelled'] = 'Abgesagt';

/*Options Lang*/
$lang['option_normal'] = 'Normal';
$lang['option_dropdown'] = 'Dropdown-Liste';
$lang['option_image'] = 'Bild';
$lang['option_video'] = 'Video';
$lang['option_iframe'] = 'Iframe';
$lang['option_enabled'] = 'aktiviert';
$lang['option_disabled'] = 'Deaktiviert';
$lang['option_ssl'] = 'SSL';
$lang['option_tls'] = 'TLS';
$lang['option_everyone'] = 'Jeder';
$lang['option_staff'] = 'MITARBEITER';
$lang['option_all'] = 'MITARBEITER - Jeder';
$lang['option_rename'] = 'Umbenennen';
$lang['option_customize'] = 'Anpassen';
$lang['option_change_faction'] = 'Fraktion ändern';
$lang['option_change_race'] = 'Klasse ändern';
$lang['option_dp'] = 'DP';
$lang['option_vp'] = 'VP';
$lang['option_dp_vp'] = 'DP & VP';
$lang['option_internal_url'] = 'Intern URL';
$lang['option_external_url'] = 'Extern URL';
$lang['option_on'] = 'Auf';
$lang['option_off'] = 'aus';

/*Count Lang*/
$lang['count_accounts_created'] = 'Konto erstellen';
$lang['count_accounts_banned'] = 'gesperrtes Konto';
$lang['count_news_created'] = 'Nachrichten erstellt';
$lang['count_changelogs_created'] = 'Änderungsprotokoll erstellet';
$lang['total_accounts_registered'] = 'Gesamtzahl der registrierten Konten.';
$lang['total_accounts_banned'] = 'Insgesamt gesperrte Konten.';
$lang['total_news_writed'] = 'Insgesamt geschriebene Nachrichten.';
$lang['total_changelogs_writed'] = 'ingesamt Änderungsprotokoll geschrieben.';

$lang['info_alliance_players'] = 'Allianzspieler';
$lang['info_alliance_playing'] = 'Allianzspieler auf realm';
$lang['info_horde_players'] = 'Hordespieler';
$lang['info_horde_playing'] = 'Hordespieler auf realm';
$lang['info_players_playing'] = 'Spieler auf dem Realm';

/*Alert Lang*/
$lang['alert_smtp_activation'] = 'Wenn Sie diese Option aktivieren, müssen Sie SMTP zum Senden von E-Mails konfigurieren.';
$lang['alert_banned_reason'] = 'Ist verboten, Grund:';

/*Logs Lang*/
$lang['log_new_level'] = 'Erhalte ein neues Level';
$lang['log_old_level'] = 'Bevor es war';
$lang['log_new_name'] = 'Es hat einen neuen Namen';
$lang['log_old_name'] = 'Bevor es war';
$lang['log_unbanned'] = 'nicht gesperrt';
$lang['log_customization'] = 'Holen Sie sich eine Anpassung';
$lang['log_change_race'] = 'ändern sie ihre Rasse.';
$lang['log_change_faction'] = 'ändern sie ihre Fraktion';
$lang['log_banned'] = 'war Gespert';
$lang['log_gm_assigned'] = 'GM-Rang erhalten';
$lang['log_gm_removed'] = 'Der GM-Rang wurde entfernt';

/*CMS Lang*/
$lang['cms_version_currently'] = 'Diese Version läuft derzeit';
$lang['cms_warning_update'] = 'Wenn der cms aktualisiert wird, kann die Konfiguration abhängig von den an jeder Version vorgenommenen Änderungen auf den Standardwert zurückgesetzt werden.';
$lang['cms_php_version'] = 'PHP Version';
$lang['cms_allow_fopen'] = 'allow_url_fopen';
$lang['cms_allow_include'] = 'allow_url_include';
$lang['cms_loaded_modules'] = 'Loaded Modules';
$lang['cms_loaded_extensions'] = 'Loaded Extensions';
