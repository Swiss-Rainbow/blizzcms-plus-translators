<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Notification Title Lang*/
$lang['notification_title_success'] = 'Erfolgreich';
$lang['notification_title_warning'] = 'Warnung';
$lang['notification_title_error'] = 'Error';
$lang['notification_title_info'] = 'Information';

/*Notification Message (Login/Register) Lang*/
$lang['notification_Benutzername_empty'] = 'Benutzername ist leer';
$lang['notification_email_empty'] = 'Email ist leer';
$lang['notification_password_empty'] = 'Passwort ist leer';
$lang['notification_user_error'] = 'Der Benutzername oder Passwort ist falsch, bitte versuche es erneut';
$lang['notification_email_error'] = 'Die Email oder Passwort ist falsch, bitte versuche es erneut';
$lang['notification_check_email'] = 'Der Benutzername oder Email ist falsch, bitte versuche es erneut';
$lang['notification_checking'] = 'Überprüfen...';
$lang['notification_redirection'] = 'Mit deinem Konto verbinden...';
$lang['notification_new_account'] = 'Neues Konto erstellt. Umleitung zur Anmeldung...';
$lang['notification_email_gesendet'] = 'Email gesendet, überprüfe dein Email Postfach';
$lang['notification_account_activation'] = 'Email gesendet. ÃœberprÃ¼fe deine Emails fÃ¼r die Aktivierung deines Kontos.';
$lang['notification_captcha_error'] = 'Email gesendet, überprüfe dein Email Postfach';
$lang['notification_password_lenght_error'] = 'Falsche Passwortlänge. bitte wähle ein Passwort zwischen 5 und 16 Zeichen';
$lang['notification_account_already_exist'] = 'Dieses Konto ist bereits vorhanden';
$lang['notification_password_not_match'] = 'Passwörter stimmen nicht überein';
$lang['notification_same_password'] = 'Das Passwort ist gleich.';
$lang['notification_currentpass_not_match'] = 'Das alte Passwort stimmt nicht überein';
$lang['notification_used_email'] = 'Email ist bereits in Benutzung';
$lang['notification_email_not_match'] = 'Email stimmt nicht überein';
$lang['notification_expansion_not_found'] = 'Erweiterung nicht gefunden';
$lang['notification_valid_key'] = 'Konto aktiviert';
$lang['notification_valid_key_desc'] = 'Jetzt kannst du dich mit deinem Konto anmelden.';
$lang['notification_invalid_key'] = 'Dieser Aktivierungsschlüssel ist ungültig.';

/*Notification Message (Admin) Lang*/
$lang['notification_donation_added'] = 'Hinzugefügte Spenden';
$lang['notification_donation_deleted'] = 'Gelöschte Spenden';
$lang['notification_donation_updated'] = 'Geänderte Spenden';
$lang['notification_name_empty'] = 'Name ist leer';
$lang['notification_points_empty'] = 'Punkte sind leer';
$lang['notification_tax_empty'] = 'Tax ist leer';
$lang['notification_price_empty'] = 'Preis ist leer';
$lang['notification_incorrect_update'] = 'Unerwartetes Update';
$lang['notification_news_deleted'] = 'Gelöschte Neuigkeiten';
$lang['notification_news_updated'] = 'Geänderte Neuigkeiten';
$lang['notification_title_empty'] = 'Überschrift ist leer';
$lang['notification_description_empty'] = 'Beschreibung ist leer';
$lang['notification_category_added'] = 'Hinzugefügte Kategorien';
$lang['notification_category_deleted'] = 'Gelöschte Kategorien';
$lang['notification_category_updated'] = 'Geänderte Kategorien';
