<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Notification Title Lang*/
$ lang ['notice_title_success'] = 'Framgång';
$ lang ['notice_title_warning'] = 'Varning';
$ lang ['notice_title_error'] = 'Fel';
$ lang ['notice_title_info'] = 'Information';

/*Notification Message (Login/Register) Lang*/
$ lang ['notice_username_empty'] = 'Användarnamn är tomt';
$ lang ['notice_email_empty'] = 'E-post är tom';
$ lang ['notice_password_empty'] = 'Lösenordet är tomt';
$ lang ['notice_user_error'] = 'Användarnamnet eller lösenordet är felaktigt. var god försök igen!';
$ lang ['notice_email_error'] = 'E-postmeddelandet eller lösenordet är felaktigt. var god försök igen!';
$ lang ['notice_check_email'] = 'Användarnamnet eller e-postadressen är felaktig. var god försök igen!';
$ lang ['notice_checking'] = 'Kontrollerar ...';
$ lang ['notice_redirection'] = 'Ansluter till ditt konto ...';
$ lang ['notice_new_account'] = 'Nytt konto skapades. omdirigerar till inloggning ... ';
$ lang ['notice_email_sent'] = 'E-post skickat. Kontrollera din e-post...';
$ lang ['notice_account_activation'] = 'E-post skickat. kontrollera din e-post för att aktivera ditt konto. ';
$ lang ['notice_captcha_error'] = 'Kontrollera captcha';
$ lang ['notice_password_lenght_error'] = 'Fel lösenordslängd. använd ett lösenord mellan 5 och 16 tecken ';
$ lang ['notice_account_already_exist'] = 'Det här kontot finns redan';
$ lang ['notice_password_not_match'] = 'Lösenord matchar inte';
$ lang ['notice_same_password'] = 'Lösenordet är detsamma.';
$ lang ['notice_currentpass_not_match'] = 'Gammalt lösenord matchar inte';
$ lang ['notice_used_email'] = 'E-post i bruk';
$ lang ['notice_email_not_match'] = 'E-post matchar inte';
$ lang ['notice_expansion_not_found'] = 'Utvidgning hittades inte';
$ lang ['notice_valid_key'] = 'Konto aktiverat';
$ lang ['notice_valid_key_desc'] = 'Nu kan du logga in med ditt konto.';
$ lang ['notice_invalid_key'] = 'Den angivna aktiveringsnyckeln är inte giltig.';

/*Notification Message (General) Lang*/
$ lang ['notice_email_chang'] = 'E-postmeddelandet har ändrats.';
$ lang ['notice_password_chang'] = 'Lösenordet har ändrats.';
$ lang ['notice_avatar_chang'] = 'Avataren har ändrats.';
$ lang ['notice_wrong_values'] = 'Värdena är fel';
$ lang ['notice_select_type'] = 'Välj en typ';
$ lang ['notice_select_priority'] = 'Välj en prioritet';
$ lang ['notice_select_category'] = 'Välj en kategori';
$ lang ['notice_select_realm'] = 'Välj en realm';
$ lang ['notice_select_character'] = 'Välj ett tecken';
$ lang ['notice_select_item'] = 'Välj en artikel';
$ lang ['notice_report_created'] = 'Rapporten har skapats.';
$ lang ['notice_title_empty'] = 'Titeln är tom';
$ lang ['notification_description_empty'] = 'Beskrivningen är tom';
$ lang ['notice_name_empty'] = 'Namnet är tomt';
$ lang ['notice_id_empty'] = 'ID är tomt';
$ lang ['notice_reply_empty'] = 'Svaret är tomt';
$ lang ['notice_reply_created'] = 'Svaret har skickats.';
$ lang ['notice_reply_deleted'] = 'Svaret har tagits bort.';
$ lang ['notice_topic_created'] = 'Ämnet har skapats.';
$ lang ['varsel_donation_successful '] =' Donationen har slutförts, kontrollera dina givarpoäng i ditt konto. ';
$ lang ['notice_donation_canceled'] = 'Donationen har avbrutits.';
$ lang ['notification_donation_error'] = 'Informationen i transaktionen stämmer inte över.';
$ lang ['notice_store_chars_error'] = 'Välj ditt tecken i varje objekt.';
$ lang ['notice_store_item_insufficient_points'] = 'Du har inte tillräckligt med poäng att köpa.';
$ lang ['notice_store_item_purchased'] = 'Objekten har köpts, kontrollera din e-post i spelet.';
$ lang ['notice_store_item_added'] = 'Det valda objektet har lagts till i din kundvagn.';
$ lang ['notice_store_item_removed'] = 'Det valda objektet har tagits bort från din kundvagn.';
$ lang ['notice_store_cart_error'] = 'Uppdateringen av vagnen misslyckades, försök igen.';

/*Notification Message (Admin) Lang*/
$ lang ['notice_changelog_created'] = 'Ändringsloggen har skapats.';
$ lang ['notice_changelog_edited'] = 'Ändringsloggen har redigerats.';
$ lang ['notice_changelog_deleted'] = 'Ändringsloggen har tagits bort.';
$ lang ['notice_forum_created'] = 'Forumet har skapats.';
$ lang ['notice_forum_edited'] = 'Forumet har redigerats.';
$ lang ['notice_forum_deleted'] = 'Forumet har tagits bort.';
$ lang ['notice_category_created'] = 'Kategorin har skapats.';
$ lang ['notice_category_edited'] = 'Kategorin har redigerats.';
$ lang ['notice_category_deleted'] = 'Kategorin har tagits bort.';
$ lang ['notice_menu_created'] = 'Menyn har skapats.';
$ lang ['notice_menu_edited'] = 'Menyn har redigerats.';
$ lang ['notice_menu_deleted'] = 'Menyn har tagits bort.';
$ lang ['notice_news_deleted'] = 'Nyheten har tagits bort.';
$ lang ['notice_page_created'] = 'Sidan har skapats.';
$ lang ['notice_page_edited'] = 'Sidan har redigerats.';
$ lang ['notice_page_deleted'] = 'Sidan har tagits bort.';
$ lang ['notice_realm_created'] = 'Realmen har skapats.';
$ lang ['notice_realm_edited'] = 'Realmen har redigerats.';
$ lang ['notice_realm_deleted'] = 'Realmen har raderats.';
$ lang ['notice_slide_created'] = 'Bildspelet har skapats.';
$ lang ['notice_slide_edited'] = 'Bilden har redigerats.';
$ lang ['notice_slide_deleted'] = 'Bildspelet har tagits bort.';
$ lang ['notice_item_created'] = 'Objektet har skapats.';
$ lang ['notice_item_edited'] = 'Objektet har redigerats.';
$ lang ['notice_item_deleted'] = 'Objektet har tagits bort.';
$ lang ['notice_top_created'] = 'Det översta objektet har skapats.';
$ lang ['notice_top_edited'] = 'Det översta objektet har redigerats.';
$ lang ['notice_top_deleted'] = 'Det översta objektet har tagits bort.';
$ lang ['notice_topsite_created'] = 'Toppsidan har skapats.';
$ lang ['notice_topsite_edited'] = 'Toppsidan har redigerats.';
$ lang ['notice_topsite_deleted'] = 'Toppsidan har tagits bort.';

$ lang ['notice_settings_updated'] = 'Inställningarna har uppdaterats.';
$ lang ['notice_module_enabled'] = 'Modulen har aktiverats.';
$ lang ['notice_module_disabled'] = 'Modulen har inaktiverats.';
$ lang ['notice_migration'] = 'Inställningarna har ställts in.';

$ lang ['notice_donation_added'] = 'Tillagd donation';
$ lang ['notice_donation_deleted'] = 'Raderad donation';
$ lang ['notice_donation_updated'] = 'Uppdaterad donation';
$ lang ['notice_points_empty'] = 'Poäng är tomma';
$ lang ['notice_tax_empty'] = 'Skatt är tom';
$ lang ['notice_price_empty'] = 'Priset är tomt';
$ lang ['notice_incorrect_update'] = 'Oväntad uppdatering';

$ lang ['notice_route_inuse'] = 'Rutten är redan i bruk, välj en annan.';

$ lang ['notice_account_updated'] = 'Kontot har uppdaterats.';
$ lang ['notice_dp_vp_empty'] = 'DP/VP är tom';
$ lang ['notice_account_banned'] = 'Kontot har förbjudits.';
$ lang ['notice_reason_empty'] = 'Anledningen är tom';
$ lang ['notice_account_ban_remove'] = 'Förbudet på kontot har tagits bort.';
$ lang ['notice_rank_empty'] = 'Rang är tom';
$ lang ['notice_rank_granted'] = 'Rang har beviljats.';
$ lang ['notice_rank_removed'] = 'Rang har raderats.';

$ lang ['notice_cms_updated'] = 'CMS har uppdaterats';
$ lang ['notice_cms_update_error'] = 'CMS kunde inte uppdateras';
$ lang ['notice_cms_not_updated'] = 'En ny version har inte hittats uppdateras';

$ lang ['notice_select_category'] = 'Det är inte underkategori';