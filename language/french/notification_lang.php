<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * BlizzCMS
 *
 * An Open Source CMS for "World of Warcraft"
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2017 - 2019, WoW-CMS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author  WoW-CMS
 * @copyright  Copyright (c) 2017 - 2019, WoW-CMS.
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://wow-cms.com
 * @since   Version 1.0.1
 * @filesource
 */

/*Notification Title Lang*/
$lang['notification_title_success'] = 'Success';
$lang['notification_title_warning'] = 'Warning';
$lang['notification_title_error'] = 'Error';
$lang['notification_title_info'] = 'Information';

/*Notification Message (Login/Register) Lang*/
$lang['notification_username_empty'] = 'Username is empty';
$lang['notification_email_empty'] = 'Email is empty';
$lang['notification_password_empty'] = 'Password is empty';
$lang['notification_user_error'] = 'The username or password is incorrect. please try again!';
$lang['notification_email_error'] = 'The email or password is incorrect. please try again!';
$lang['notification_check_email'] = 'The username or email is incorrect. please try again!';
$lang['notification_checking'] = 'Checking...';
$lang['notification_redirection'] = 'Connecting to your account...';
$lang['notification_new_account'] = 'New account created. redirecting to login...';
$lang['notification_email_sent'] = 'Email sent. please check your email...';
$lang['notification_account_activation'] = 'Email sent. please check your email for activate your account.';
$lang['notification_captcha_error'] = 'Please check the captcha';
$lang['notification_password_lenght_error'] = 'Wrong password length. please use a password between 5 and 16 characters';
$lang['notification_account_already_exist'] = 'This account already exists';
$lang['notification_password_not_match'] = 'Passwords do not match';
$lang['notification_same_password'] = 'The password is the same.';
$lang['notification_currentpass_not_match'] = 'Old Password do not match';
$lang['notification_used_email'] = 'Email in use';
$lang['notification_email_not_match'] = 'Email do not match';
$lang['notification_expansion_not_found'] = 'Expansion not found';
$lang['notification_valid_key'] = 'Account Activated';
$lang['notification_valid_key_desc'] = 'Now you can sign in with your account.';
$lang['notification_invalid_key'] = 'The activation key provided is not valid.';

/*Notification Message (Admin) Lang*/
$lang['notification_donation_added'] = 'Added donation';
$lang['notification_donation_deleted'] = 'Deleted donation';
$lang['notification_donation_updated'] = 'Updated donation';
$lang['notification_name_empty'] = 'Name is empty';
$lang['notification_points_empty'] = 'Points is empty';
$lang['notification_tax_empty'] = 'Tax is empty';
$lang['notification_price_empty'] = 'Price is empty';
$lang['notification_incorrect_update'] = 'Unexpected update';
$lang['notification_news_deleted'] = 'News deleted';
$lang['notification_news_updated'] = 'News updated';
$lang['notification_title_empty'] = 'Title is Empty';
$lang['notification_description_empty'] = 'Description is Empty';
$lang['notification_category_added'] = 'Category added';
$lang['notification_category_deleted'] = 'Category deleted';
$lang['notification_category_updated'] = 'Category updated';
